package regio_vinco;

import audio_manager.AudioManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import pacg.PointAndClickGame;
import pacg.PointAndClickGameDataModel;
import static regio_vinco.RegioVinco.*;
import world_data.Region;

/**
 * This class manages the game data for the Regio Vinco game application. Note
 * that this game is built using the Point & Click Game Framework as its base. 
 * This class contains methods for managing game data and states.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class RegioVincoDataModel extends PointAndClickGameDataModel {
    // THIS IS THE MAP IMAGE THAT WE'LL USE
    private WritableImage mapImage;
    private PixelReader mapPixelReader;
    private PixelWriter mapPixelWriter;
    
    // AND OTHER GAME DATA
    private String regionName;
    private String subRegionsType;
    private HashMap<Color, String> colorToSubRegionMappings;
    private HashMap<String, Color> subRegionToColorMappings;
    private HashMap<String, ArrayList<int[]>> pixels;
    private LinkedList<String> redSubRegions;
    private LinkedList<MovableText> subRegionStack;
    
    int redCounter;
    Label timeLabel = new Label();
    long startTime;
    long endTime;
    String duration;
    
    boolean stackEmpty = false;
    
    ReentrantLock lock = new ReentrantLock();

    /**
     * Default constructor, it initializes all data structures for managing the
     * Sprites, including the map.
     */
    public RegioVincoDataModel() {
	// INITIALIZE OUR DATA STRUCTURES
	colorToSubRegionMappings = new HashMap();
	subRegionToColorMappings = new HashMap();
	subRegionStack = new LinkedList();
	redSubRegions = new LinkedList();
    }
    
    public void setMapImage(WritableImage initMapImage) {
	mapImage = initMapImage;
	mapPixelReader = mapImage.getPixelReader();
	mapPixelWriter = mapImage.getPixelWriter();
    }

    public void removeAllButOneFromeStack(RegioVincoGame game) {
        lock.lock();
        try {
            while (subRegionStack.size() > 1) {
                MovableText text = subRegionStack.removeFirst();
                ((RegioVincoGame)game).gameLayer.getChildren().remove(text.getLabel());
                String subRegionName = text.getText().getText();

                // TURN THE TERRITORY GREEN
                changeSubRegionColorOnMap(game, subRegionName, Color.web("669966"));
            }
            changeSubRegionColorOnMap(game, subRegionStack.getFirst().getText().getText(), getColorMappedToSubRegion(subRegionStack.getFirst().getText().getText()));
            game.cCounter();
            startTextStackMovingDown();
        }
        finally
        {
            lock.unlock();
        }
    }

    // ACCESSOR METHODS
    public String getRegionName() {
	return regionName;
    }

    public String getSubRegionsType() {
	return subRegionsType;
    }

    public void setRegionName(String initRegionName) {
	regionName = initRegionName;
    }

    public void setSubRegionsType(String initSubRegionsType) {
	subRegionsType = initSubRegionsType;
    }

    public String getSecondsAsTimeText(long numSeconds) {
	long numHours = numSeconds / 3600;
	numSeconds = numSeconds - (numHours * 3600);
	long numMinutes = numSeconds / 60;
	numSeconds = numSeconds - (numMinutes * 60);

	String timeText = "";
	if (numHours > 0) {
	    timeText += numHours + ":";
	}
	timeText += numMinutes + ":";
	if (numSeconds < 10) {
	    timeText += "0" + numSeconds;
	} else {
	    timeText += numSeconds;
	}
	return timeText;
    }

    public int getRegionsFound() {
	return colorToSubRegionMappings.keySet().size() - subRegionStack.size();
    }

    public int getRegionsNotFound() {
	return subRegionStack.size();
    }
    
    public LinkedList<MovableText> getSubRegionStack() {
	return subRegionStack;
    }
    
    public String getSubRegionMappedToColor(Color colorKey) {
	return colorToSubRegionMappings.get(colorKey);
    }
    
    public Color getColorMappedToSubRegion(String subRegion) {
	return subRegionToColorMappings.get(subRegion);
    }

    // MUTATOR METHODS

    public void addColorToSubRegionMappings(Color colorKey, String subRegionName) {
	colorToSubRegionMappings.put(colorKey, subRegionName);
    }

    public void addSubRegionToColorMappings(String subRegionName, Color colorKey) {
	subRegionToColorMappings.put(subRegionName, colorKey);
    }
    public Color getSelectedColor(int x, int y)
    {
        return mapPixelReader.getColor(x, y);
    }
    public void respondToMapSelection(RegioVincoGame game, int x, int y) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	Color pixelColor = mapPixelReader.getColor(x, y);
	String clickedSubRegion = colorToSubRegionMappings.get(pixelColor);
        System.out.println(clickedSubRegion);
	if ((clickedSubRegion == null) || (subRegionStack.isEmpty())) {
	    return;
	}
	if (clickedSubRegion.equals(subRegionStack.get(0).getText().getText())) {
	    // YAY, CORRECT ANSWER
            if (game.getSoundState()==true)
            {
                game.getAudio().play(SUCCESS, false);
            }

	    // TURN THE TERRITORY GREEN
	    changeSubRegionColorOnMap(game, clickedSubRegion, Color.web("669966"));

	    // REMOVE THE BOTTOM ELEMENT FROM THE STACK
	    
           ((RegioVincoGame)game).gameLayer.getChildren().remove(subRegionStack.getFirst().getLabel());
           subRegionStack.removeFirst();
           
	    // AND LET'S CHANGE THE RED ONES BACK TO THEIR PROPER COLORS
	    for (String s : redSubRegions) {
		Color subRegionColor = subRegionToColorMappings.get(s);
		changeSubRegionColorOnMap(game, s, subRegionColor);
	    }
	    redSubRegions.clear();

	    startTextStackMovingDown();

	    if (subRegionStack.isEmpty()) {
		this.endGameAsWin();
		game.getAudio().stop(TRACKED_SONG);
                try {
                game.getAudio().loadAudio(game.getCurrentRegion(), game.getCurrentRegionSongFile());
                } catch (Exception e) {
	    
                }
		//game.getAudio().play(AFGHAN_ANTHEM, false);
                if(((RegioVincoGame) game).getMusicState()==true)
                {
                    if((new File(game.getCurrentRegionSongFile())).exists())
                    {
                        game.getAudio().play(game.getCurrentRegion(), true);
                    }
                    else
                    {
                        game.getAudio().play(AFGHAN_ANTHEM, false);
                    }
                }
                endTime = (System.currentTimeMillis()- startTime)/1000;
                duration = timeLabel.getText();
                
	    }
	} else {
	    if (!redSubRegions.contains(clickedSubRegion)) {
		// BOO WRONG ANSWER
                if (game.getSoundState()==true)
                {
                    game.getAudio().play(FAILURE, false);
                }

		// TURN THE TERRITORY TEMPORARILY RED
		changeSubRegionColorOnMap(game, clickedSubRegion, Color.web("ca4848"));
		redSubRegions.add(clickedSubRegion);
                redCounter++;
	    }
	}
    }

    public void startTextStackMovingDown() {
	// AND START THE REST MOVING DOWN
	for (MovableText mT : subRegionStack) {
	    mT.setVelocityY(SUB_STACK_VELOCITY);
	}
    }

    public void changeSubRegionColorOnMap(RegioVincoGame game, String subRegion, Color color) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
	for (int[] pixel : subRegionPixels) {
	    mapPixelWriter.setColor(pixel[0], pixel[1], color);
	}   
    }

    public int getNumberOfSubRegions() {
	return colorToSubRegionMappings.keySet().size();
    }
    public int getIncorrect()
    {
        return redCounter;
    }
    /**
     * Resets all the game data so that a brand new game may be played.
     *
     * @param game the Zombiquarium game in progress
     */
    @Override
    public void reset(PointAndClickGame game, String fileName) {

	// THIS GAME ONLY PLAYS AFGHANISTAN
	regionName = "World";
	subRegionsType = "Continents";

	// LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();
        redCounter = 0;

        // INIT THE MAPPINGS - NOTE THIS SHOULD 
	// BE DONE IN A FILE, WHICH WE'LL DO IN
	// FUTURE HOMEWORK ASSIGNMENTS
	
	//colorToSubRegionMappings.put(makeColor(134, 134, 134), "    Kandahar");
        //colorToSubRegionMappings.
        File file = new File("./data/The World/The World Data.xml");
        if(fileName.equals("Asia") || fileName.equals("Antarctica") || fileName.equals("Europe") || fileName.equals("Oceania") || fileName.equals("North America") || fileName.equals("South America") || fileName.equals("Africa"))
        {
            file = new File("./data/The World/" + fileName + "/" + fileName + " Data.xml");
        }
        else if(((RegioVincoGame) game).countryState())
        {
            file = new File("./data/The World/" + ((RegioVincoGame) game).getContinent() + "/" + fileName + "/" + fileName + " Data.xml");
        }
        
        if(((RegioVincoGame) game).getWorldDataManager().load(file))
        {   
            HashMap<String, Region> map = ((RegioVincoGame) game).getWorldDataManager().getMap();
            for (HashMap.Entry<String, Region> entry : map.entrySet())
            {
                Region value = entry.getValue();
                if((((RegioVincoGame)game).getGameState()).equals("name"))
                {
                    colorToSubRegionMappings.put(makeColor(value.getRed(), value.getGreen(), value.getBlue()), "      "+value.getName());
                }
                else if((((RegioVincoGame)game).getGameState()).equals("flag"))
                {
                    colorToSubRegionMappings.put(makeColor(value.getRed(), value.getGreen(), value.getBlue()), value.getName());
                }
                else if((((RegioVincoGame)game).getGameState()).equals("leader"))
                {
                    colorToSubRegionMappings.put(makeColor(value.getRed(), value.getGreen(), value.getBlue()), "      "+value.getLeader());
                }
                else if((((RegioVincoGame)game).getGameState()).equals("capital"))
                {
                    colorToSubRegionMappings.put(makeColor(value.getRed(), value.getGreen(), value.getBlue()), "      "+value.getCapital());
                }
                subRegionToColorMappings.put("      "+value.getName(), makeColor(value.getRed(), value.getGreen(), value.getBlue()));
            }
        }
        
        
	// REST THE MOVABLE TEXT
        if(((RegioVincoGame) game).getGameState().equals("flag"))
        {
            Pane gameLayer = ((RegioVincoGame)game).getGameLayer();
            gameLayer.getChildren().clear();
            for (Color c : colorToSubRegionMappings.keySet()) 
            {
                String subRegion = colorToSubRegionMappings.get(c);
                if(new File("./data/The World/" + ((RegioVincoGame) game).getCurrentContinent() + "/" + subRegion + "/" + subRegion + " Flag.png").exists())
                {
                    subRegionToColorMappings.put(subRegion, c);
                    Text textNode = new Text(subRegion);
                    textNode.setScaleX(1.2);
                    textNode.setScaleY(1.2);
                    Label labelNode = new Label();
                    MovableText subRegionText = new MovableText(textNode, labelNode);
                    subRegionText.getText().setFill(Color.BLACK);
                    labelNode.setPrefSize(300, 50);
                    labelNode.setGraphic(textNode);
                    labelNode.setGraphic(new ImageView((loadImage("./data/The World/" + ((RegioVincoGame) game).getCurrentContinent() + "/" + subRegion + "/" + subRegion + " Flag.png"))));
                    int flagHeight = (int) (new Image("file:./data/The World/" + ((RegioVincoGame) game).getCurrentContinent() + "/" + subRegion + "/" + subRegion + " Flag.png")).getHeight();
                    labelNode.setStyle("-fx-background-color: rgb(" + (c.getRed()*225) + "," + (c.getGreen()*225) + "," + (c.getBlue()*255) +");");
                    labelNode.setPrefSize(300, flagHeight);
                    labelNode.setLayoutX(STACK_X);
        //	    textNode.setX(STACK_X);
                    subRegionStack.add(subRegionText);
                    gameLayer.getChildren().add(labelNode);
                }
            }
        }
        else
        {
            Pane gameLayer = ((RegioVincoGame)game).getGameLayer();
            gameLayer.getChildren().clear();
            for (Color c : colorToSubRegionMappings.keySet()) 
            {
                String subRegion = colorToSubRegionMappings.get(c);
                subRegionToColorMappings.put(subRegion, c);
                Text textNode = new Text(subRegion);
                textNode.setScaleX(1.2);
                textNode.setScaleY(1.2);
                Label labelNode = new Label();
                MovableText subRegionText = new MovableText(textNode, labelNode);
                subRegionText.getText().setFill(Color.BLACK);
                labelNode.setPrefSize(300, 50);
                labelNode.setGraphic(textNode);
                labelNode.setStyle("-fx-background-color: rgb(" + (c.getRed()*225) + "," + (c.getGreen()*225) + "," + (c.getBlue()*255) +");");
                labelNode.setLayoutX(STACK_X);
    //	    textNode.setX(STACK_X);
                subRegionStack.add(subRegionText);
                gameLayer.getChildren().add(labelNode);
            }
        }
	Collections.shuffle(subRegionStack);
        System.out.println("Stack size:" + subRegionStack.size());
        if(subRegionStack.isEmpty())
        {
            ((RegioVincoGame)game).changeFlagButton(true);
            stackEmpty = true;
            
        }
        else
        {
        stackEmpty = false;
	int y = STACK_INIT_Y;
	int yInc = STACK_INIT_Y_INC;
	// NOW FIX THEIR Y LOCATIONS
	for (MovableText mT : subRegionStack) {
	    int tY = y + yInc;
            if(((RegioVincoGame) game).getGameState().equals("flag"))
            {
                yInc -= 100;
                mT.getLabel().setLayoutY(tY-80);
            }
            else
            {
                yInc -= 50;
                mT.getLabel().setLayoutY(tY);
            }
	}

	// RELOAD THE MAP
        ((RegioVincoGame) game).reloadMap(fileName);
	//((RegioVincoGame) game).reloadMap();

	// LET'S RECORD ALL THE PIXELS
	pixels = new HashMap();
	for (MovableText mT : subRegionStack) {
	    pixels.put(mT.getText().getText(), new ArrayList());
	}

	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
		    subRegionPixels.add(pixel);
		}
	    }
	}

	// RESET THE AUDIO
	AudioManager audio = ((RegioVincoGame) game).getAudio();
	audio.stop(AFGHAN_ANTHEM);

	if (!audio.isPlaying(TRACKED_SONG)) {
            if(((RegioVincoGame) game).getMusicState()==true)
            {
                audio.play(TRACKED_SONG, true);
            }
	}
	// LET'S GO
        startTime = System.currentTimeMillis();
        if (((RegioVincoGame) game).getScoreLayer().getChildren().contains(timeLabel))
        {
            
        }
        else
        {
            timeLabel.translateXProperty().setValue(20);
            timeLabel.translateYProperty().setValue(0);
            timeLabel.setScaleX(2);
            timeLabel.setScaleY(2);
            timeLabel.setTextFill(Color.web("#000000"));
            ((RegioVincoGame) game).getScoreLayer().getChildren().add(timeLabel);
        }
        beginGame();
        }
    }
    @Override
    public void resetMap(PointAndClickGame game, String fileName) {

	// THIS GAME ONLY PLAYS AFGHANISTAN
	regionName = "World";
	subRegionsType = "Continents";

	// LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();
        redCounter = 0;

        // INIT THE MAPPINGS - NOTE THIS SHOULD 
	// BE DONE IN A FILE, WHICH WE'LL DO IN
	// FUTURE HOMEWORK ASSIGNMENTS
	
	//colorToSubRegionMappings.put(makeColor(134, 134, 134), "    Kandahar");
        //colorToSubRegionMappings.
        File file = new File("./data/The World/The World Data.xml");
        if(fileName.equals("Asia") || fileName.equals("Antarctica") || fileName.equals("Europe") || fileName.equals("Oceania") || fileName.equals("North America") || fileName.equals("South America") || fileName.equals("Africa"))
        {
            file = new File("./data/The World/" + fileName + "/" + fileName + " Data.xml");
        }
        else if(((RegioVincoGame) game).countryState())
        {
            file = new File("./data/The World/" + ((RegioVincoGame) game).getContinent() + "/" + fileName + " Data.xml");
        }
        if(((RegioVincoGame) game).getWorldDataManager().load(file))
        {
            HashMap<String, Region> map = ((RegioVincoGame) game).getWorldDataManager().getMap();
            for (HashMap.Entry<String, Region> entry : map.entrySet()) 
            {
                Region value = entry.getValue();
                colorToSubRegionMappings.put(makeColor(value.getRed(), value.getGreen(), value.getBlue()), value.getName());
            }
        }

	// RELOAD THE MAP
        ((RegioVincoGame) game).loadMapImage(fileName);
	//((RegioVincoGame) game).reloadMap(fileName);

	// LET'S RECORD ALL THE PIXELS
	pixels = new HashMap();
//	for (MovableText mT : subRegionStack) {
//	    pixels.put(mT.getText().getText(), new ArrayList());
//	}

	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
//		    subRegionPixels.add(pixel);
		}
	    }
	}
        

	// RESET THE AUDIO
	AudioManager audio = ((RegioVincoGame) game).getAudio();
	audio.stop(AFGHAN_ANTHEM);

	if (!audio.isPlaying(TRACKED_SONG)) {
	    if(((RegioVincoGame) game).getMusicState()==true)
            {
                audio.play(TRACKED_SONG, true);
            }
	}
	// LET'S GO
        //beginGame();
    }
   
    // HELPER METHOD FOR MAKING A COLOR OBJECT
    public static Color makeColor(int r, int g, int b) {
	return Color.color(r/255.0, g/255.0, b/255.0);
    }

    // STATE TESTING METHODS
    // UPDATE METHODS
	// updateAll
	// updateDebugText
    
    /**
     * Called each frame, this thread already has a lock on the data. This
     * method updates all the game sprites as needed.
     *
     * @param game the game in progress
     */
    @Override
    public void updateAll(PointAndClickGame game, double percentage) {
        timeLabel.setText(getSecondsAsTimeText((System.currentTimeMillis() - startTime)/1000));
	for (MovableText mT : subRegionStack) {
	    mT.update(percentage);
	}
	if (!subRegionStack.isEmpty()) {
            subRegionStack.getFirst().getLabel().setStyle("-fx-background-color: #669966;");
	    MovableText bottomOfStack = subRegionStack.get(0);
	    if(((RegioVincoGame)game).getGameState().equals("flag"))
            {
                double bottomY = (bottomOfStack.getLabel().getTranslateY() + bottomOfStack.getLabel().getLayoutY()) + 30;
                int flagHeight = (int) (bottomOfStack.getLabel().getHeight());
                    if (bottomY >= (730 - flagHeight)) {
                    double diffY = bottomY - (730 - flagHeight);
                    for (MovableText mT : subRegionStack) 
                    {
                        mT.getText().setY(mT.getText().getY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
            }
            else
            {
                double bottomY = (bottomOfStack.getLabel().getTranslateY() + bottomOfStack.getLabel().getLayoutY()) + 30;
                if (bottomY >= FIRST_REGION_Y_IN_STACK) {
                    double diffY = bottomY - FIRST_REGION_Y_IN_STACK;
                    for (MovableText mT : subRegionStack) {


                        mT.getText().setY(mT.getText().getY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
            }
	}
    }

    /**
     * Called each frame, this method specifies what debug text to render. Note
     * that this can help with debugging because rather than use a
     * System.out.print statement that is scrolling at a fast frame rate, we can
     * observe variables on screen with the rest of the game as it's being
     * rendered.
     *
     * @return game the active game being played
     */
    public void updateDebugText(PointAndClickGame game) {
	debugText.clear();
    }
    public int getEndTime()
    {
        return (int)endTime;
    }
    public String getDuration()
    {
        return duration;
    }
    public void clearStack()
    {
        subRegionStack.clear();
    }
    private Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }
    public boolean getStackState()
    {
        return stackEmpty;
    }
}
