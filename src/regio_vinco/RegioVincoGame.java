package regio_vinco;

import audio_manager.AudioManager;
import java.io.File;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Optional;
import world_data.WorldDataManager;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static regio_vinco.RegioVinco.*;
import static regio_vinco.RegioVincoDataModel.makeColor;
import world_data.Region;

/**
 * This class is a concrete PointAndClickGame, as specified in The PACG
 * Framework. Note that this one plays Regio Vinco.
 *
 * @author McKillaGorilla
 */
public class RegioVincoGame extends PointAndClickGame {

    // THIS PROVIDES GAME AND GUI EVENT RESPONSES
    RegioVincoController controller;

    // THIS PROVIDES MUSIC AND SOUND EFFECTS
    AudioManager audio;
    
    WorldDataManager worldData;
    
    // THESE ARE THE GUI LAYERS
    Pane backgroundLayer;
    Pane backingLayer;
    Pane gameLayer;
    Pane coverLayer;
    Pane guiLayer;
    Pane scoreLayer;
    Pane winScoreLayer;
    Pane splashLayer;
    Pane settingsLayer;
    Pane helpLayer;
    
    Label time;
    Label regionsFound;
    Label regionsLeft;
    Label incorrectGuesses;
    
    Label regionName;
    Label score;
    Label gameDuration;
    Label subRegions;
    Label wrongGuesses;
    
    String tempDistricts;
    
    Label mapName;
    String currentMap;
    
    Label mouseOverName;
    Label districts;
    Label highScore;
    Label fastestTime;
    Label fewestGuesses;
    Label flagImage;
    
    Label unplayable;
    
    Label world;
    Label continent;
    Label country;
    
    Label bottomScore;
    Label bottomBestTime;
    Label bottomLeastGuesses;
    
    Label help;
    Label settings;
    
    boolean gameStarted = false;
    boolean musicOn = true;
    boolean soundOn = true;
    
    String gameState = "off";
    
    String currentContinent;
    String currentRegion;
    
    long timer;
    
//    System.currenttime(millis);
    
    
    /**
     * Get the game setup.
     */
    public RegioVincoGame(Stage initWindow, WorldDataManager initData) {
	super(initWindow, APP_TITLE, TARGET_FRAME_RATE);
        initWindow.setHeight(GAME_HEIGHT);
        initWindow.setWidth(GAME_WIDTH);
        initWindow.setMaxHeight(GAME_HEIGHT);   
        initWindow.setMaxWidth(GAME_WIDTH);
        initWindow.setResizable(false);
	initAudio();
        worldData = initData;
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Pane getGameLayer() {
	return gameLayer;
    }

    /**
     * Initializes audio for the game.
     */
    private void initAudio() {
	audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);
	    audio.loadAudio(AFGHAN_ANTHEM, AFGHAN_ANTHEM_FILE_NAME);
	    audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
	} catch (Exception e) {
	    
	}
    }

    // OVERRIDDEN METHODS - REGIO VINCO IMPLEMENTATIONS
    // initData
    // initGUIControls
    // initGUIHandlers
    // reset
    // updateGUI
    /**
     * Initializes the complete data model for this application, forcing the
     * setting of all game data, including all needed SpriteType objects.
     */
    @Override
    public void initData() {
	// INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);

	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    /**
     * For initializing all GUI controls, specifically all the buttons and
     * decor. Note that this method must construct the canvas with its custom
     * renderer.
     */
    @Override
    public void initGUIControls() {
	// LOAD THE GUI IMAGES, WHICH INCLUDES THE BUTTONS
	// THESE WILL BE ON SCREEN AT ALL TIMES
	backgroundLayer = new Pane();
	addStackPaneLayer(backgroundLayer);
	addGUIImage(backgroundLayer, BACKGROUND_TYPE, loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
        
	// THEN BACKING LAYER
        backingLayer = new Pane();
        addStackPaneLayer(backingLayer);
        ImageView backingView = addGUIImage(backingLayer, BACK_TYPE, loadImage(STACK_BACK_FILE_PATH), BACK_X, BACK_Y);
        backingView.setVisible(false);
	
	// THEN THE GAME LAYER
	gameLayer = new Pane();
	addStackPaneLayer(gameLayer);
	
        // THEN COVER LAYER
        coverLayer = new Pane();
        addStackPaneLayer(coverLayer);
        ImageView coverView = addGUIImage(coverLayer, COVER_TYPE, loadImage(STACK_COVER_FILE_PATH), COVER_X, COVER_Y);
        coverView.setVisible(false);
        
	// THEN THE GUI LAYER
	guiLayer = new Pane();
	addStackPaneLayer(guiLayer);
	addGUIImage(guiLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), 860, 0);
	addGUIButton(guiLayer, START_TYPE, loadImage(START_BUTTON_FILE_PATH), 860, 105);
        addGUIImage(guiLayer, START_EXIT_LINE_TYPE, loadImage(START_EXIT_LINE_FILE_PATH), 1024, 106);
	addGUIButton(guiLayer, EXIT_TYPE, loadImage(EXIT_BUTTON_FILE_PATH), 1026, 105);
        addGUIButton(guiLayer, SETTINGS_TYPE, loadImage(SETTINGS_BUTTON_FILE_PATH), 1148, 9);
        addGUIButton(guiLayer, HELP_TYPE, loadImage(HELP_BUTTON_FILE_PATH), 1149, 54);
        
        Button nameGameButton = addGUIButton(guiLayer, NAME_GAME_TYPE, loadImage(NAME_GAME_BUTTON_FILE_PATH), 870, 108);
        Button capitalGameButton = addGUIButton(guiLayer, CAPITAL_GAME_TYPE, loadImage(CAPITAL_GAME_BUTTON_FILE_PATH), 935, 108);
        Button flagGameButton = addGUIButton(guiLayer, FLAG_GAME_TYPE, loadImage(FLAG_GAME_BUTTON_FILE_PATH), 1000, 108);
        Button leaderGameButton = addGUIButton(guiLayer, LEADER_GAME_TYPE, loadImage(LEADER_GAME_BUTTON_FILE_PATH), 1065, 108);
        Button stopGameButton = addGUIButton(guiLayer, STOP_GAME_TYPE, loadImage(STOP_GAME_BUTTON_FILE_PATH), 1130, 108);
	
        nameGameButton.setVisible(false);
        capitalGameButton.setVisible(false);
        flagGameButton.setVisible(false);
        leaderGameButton.setVisible(false);
        stopGameButton.setVisible(false);
        
	// NOTE THAT THE MAP IS ALSO AN IMAGE, BUT
	// WE'LL LOAD THAT WHEN A GAME STARTS, SINCE
	// WE'LL BE CHANGING THE PIXELS EACH TIME
	// FOR NOW WE'LL JUST LOAD THE ImageView
	// THAT WILL STORE THAT IMAGE
	ImageView mapView = new ImageView();
	mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
	guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView);
        
        mapName = new Label("MAP");
        guiLayer.getChildren().add(mapName);
        setLabelStyle(mapName, 400, 30, "#FFFFFF"); 
        mapName.setScaleX(5);
        mapName.setScaleY(5);
        
        mouseOverName = new Label("");
        districts = new Label("Districts: N/A");
        highScore = new Label("Highscore: N/A");
        fastestTime = new Label("Fastest Time: N/A");
        fewestGuesses = new Label("Fewest Guesses: N/A");
        flagImage = new Label();
        unplayable = new Label("Un-playable Region");
        guiLayer.getChildren().add(mouseOverName);
        guiLayer.getChildren().add(districts);
        guiLayer.getChildren().add(highScore);
        guiLayer.getChildren().add(fastestTime);
        guiLayer.getChildren().add(fewestGuesses);
        guiLayer.getChildren().add(flagImage);
        guiLayer.getChildren().add(unplayable);
        setLabelStyle(mouseOverName, 980, 200, "#000000");
        setLabelStyle(districts, 980, 230, "#000000");
        setLabelStyle(highScore, 980, 260, "#000000");
        setLabelStyle(fastestTime, 980, 290, "#000000");
        setLabelStyle(fewestGuesses, 980, 320, "#000000");
        flagImage.translateXProperty().setValue(930);
        flagImage.translateYProperty().setValue(400);
        setLabelStyle(unplayable, 980, 200, "#000000");
        mouseOverName.setVisible(false);
	districts.setVisible(false);
        highScore.setVisible(false);
        fastestTime.setVisible(false);
        fewestGuesses.setVisible(false);
        flagImage.setVisible(false);
        unplayable.setVisible(false);
        
        world = new Label("World");
        continent = new Label("Continent");
        country = new Label("Country");
        guiLayer.getChildren().add(world);
        guiLayer.getChildren().add(continent);
        guiLayer.getChildren().add(country);
        setLabelStyle(world, 50, 670, "#000000");
        setLabelStyle(continent, 200, 670, "#000000");
        setLabelStyle(country, 350, 670, "#000000");
        world.setVisible(false);
        continent.setVisible(false);
        country.setVisible(false);
        bottomScore = new Label("Highscore: N/A");
        bottomBestTime = new Label("Fastest Time: N/A");
        bottomLeastGuesses = new Label("Fewest Guesses: N/A");
        guiLayer.getChildren().add(bottomScore);
        guiLayer.getChildren().add(bottomBestTime);
        guiLayer.getChildren().add(bottomLeastGuesses);
        setLabelStyle(bottomScore, 60, 640, "#000000");
        setLabelStyle(bottomBestTime, 260, 640, "#000000");
        setLabelStyle(bottomLeastGuesses, 500, 640, "#000000");
        
        // NOW LOAD THE WIN DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	ImageView winView = addGUIImage(guiLayer, WIN_DISPLAY_TYPE, loadImage(WIN_DISPLAY_FILE_PATH), WIN_X, WIN_Y);
	winView.setVisible(false);
        
        ImageView provinceView = addGUIImage(guiLayer, PROVINCE_TYPE, loadImage(PROVINCES_FILE_PATH), 920, 190);
	provinceView.setVisible(false);
        
        scoreLayer = new Pane();
        addStackPaneLayer(scoreLayer);
        scoreLayer.setPrefSize(50, 50);
        scoreLayer.setTranslateX(0);
        scoreLayer.setTranslateY(670);
        
        //time = new Label();
        regionsFound = new Label("Regions Found: 0");
        regionsLeft = new Label("Regions Left: 34");
        incorrectGuesses = new Label("Incorrect Guesses: 0");
        
        //scoreLayer.getChildren().add(time);
        scoreLayer.getChildren().add(regionsFound);
        scoreLayer.getChildren().add(regionsLeft);
        scoreLayer.getChildren().add(incorrectGuesses);
        
        //setLabelStyle(time, 20, 0, "#FFFFFF");
        setLabelStyle(regionsFound, 170, 0, "#000000");        
        setLabelStyle(regionsLeft, 400, 0, "#000000");        
        setLabelStyle(incorrectGuesses, 630, 0, "#000000");
        
        scoreLayer.setVisible(false);
        
        winScoreLayer = new Pane();
        addStackPaneLayer(winScoreLayer);
        winScoreLayer.setPrefSize(50, 50);
        winScoreLayer.setTranslateX(0);
        winScoreLayer.setTranslateY(250);
        
        regionName = new Label("Region: " + currentRegion);
        score = new Label("Score: 0");
        gameDuration = new Label("Game Duration: 0:00");
        subRegions = new Label("Sub-Regions: 34");
        wrongGuesses = new Label("Incorrect Guesses: 0");
                
        winScoreLayer.getChildren().add(regionName);
        winScoreLayer.getChildren().add(score);
        winScoreLayer.getChildren().add(gameDuration);
        winScoreLayer.getChildren().add(subRegions);
        winScoreLayer.getChildren().add(wrongGuesses);
        
        setLabelStyle(regionName, 540, 20, "#A0A0A0");
        setLabelStyle(score, 540, 60, "#A0A0A0");
        setLabelStyle(gameDuration, 540, 100, "#A0A0A0");
        setLabelStyle(subRegions, 540, 140, "#A0A0A0");
        setLabelStyle(wrongGuesses, 540, 180, "#A0A0A0");
    
        winScoreLayer.setVisible(false);
        
        settingsLayer = new Pane();
        addStackPaneLayer(settingsLayer);
        addGUIImage(settingsLayer, SETTINGS_PANE_TYPE, loadImage(SETTINGS_FILE_PATH), 0, 0);
        addGUIImage(settingsLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), 860, 0);
        addGUIButton(settingsLayer, MAP1_BUTTON_TYPE, loadImage(MAP_BUTTON_FILE_PATH), 1149, 10);
        addGUIButton(settingsLayer, HELP1_TYPE, loadImage(HELP_BUTTON_FILE_PATH), 1149, 54);
        addGUIButton(settingsLayer, SOUND1_TYPE, loadImage(SOUND_ON_FILE_PATH), 625, 241);
        addGUIButton(settingsLayer, SOUND2_TYPE, loadImage(SOUND_ON_FILE_PATH), 625, 409);
        settingsLayer.setVisible(false);
        
        helpLayer = new Pane();
        addStackPaneLayer(helpLayer);
        addGUIImage(helpLayer, BLANK_TYPE, loadImage(BLANK_FILE_PATH), 0, 0);
        addGUIImage(helpLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), 860, 0);
        addGUIButton(helpLayer, SETTINGS2_TYPE, loadImage(SETTINGS_BUTTON_FILE_PATH), 1148, 9);
        addGUIButton(helpLayer, MAP2_BUTTON_TYPE, loadImage(MAP_BUTTON_FILE_PATH), 1149, 54);
        help = new Label("Here you are, planning a trip to South America,\n"
                        + "but before you jump on a plane you want to know\n"
                        + "the lay of the land. Maybe you learned a bit about\n"
                        + "the continent in school, maybe you’ve forgotten.\n"
                        + "In either case you need to make sure you’re caught\n"
                        + "up on South American geography before you touch down.\n"
                        + "Where can you go to get a refresher course on South \n"
                        + "American borders, capitals, flags, and leaders?\n"
                        + "Regio Vinco of course!");
        helpLayer.getChildren().add(help);
        setLabelStyle(help, 250, 250, "#000000");
        helpLayer.setVisible(false);
        
        splashLayer = new Pane();
        addStackPaneLayer(splashLayer);
        addGUIImage(splashLayer, SPLASH_TYPE, loadImage(SPLASH_FILE_PATH), SPLASH_X, SPLASH_Y);
        addGUIButton(splashLayer, ENTER_TYPE, loadImage(ENTER_BUTTON_FILE_PATH), 480, 422);
//	addGUIButton(guiLayer, START_TYPE, loadImage(START_BUTTON_FILE_PATH), 860, 105);
//	addGUIButton(guiLayer, EXIT_TYPE, loadImage(EXIT_BUTTON_FILE_PATH), 1026, 105);
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    private Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }

    /**
     * For initializing all the button handlers for the GUI.
     */
    @Override
    public void initGUIHandlers() {
	controller = new RegioVincoController(this);

        ImageView startExitLine = guiImages.get(START_EXIT_LINE_TYPE);
        
	Button startButton = guiButtons.get(START_TYPE);
        Button exitButton = guiButtons.get(EXIT_TYPE);
        
        Button nameGameButton = guiButtons.get(NAME_GAME_TYPE);
        Button capitalGameButton = guiButtons.get(CAPITAL_GAME_TYPE);
        Button flagGameButton = guiButtons.get(FLAG_GAME_TYPE);
        Button leaderGameButton = guiButtons.get(LEADER_GAME_TYPE);
        Button stopGameButton = guiButtons.get(STOP_GAME_TYPE);
        
        startButton.setStyle("-fx-background-color: transparent;");
	startButton.setOnAction(e -> {
            startButton.setVisible(false);
            exitButton.setVisible(false);
            startExitLine.setVisible(false);
            
            nameGameButton.setVisible(true);
            capitalGameButton.setVisible(true);
            flagGameButton.setVisible(true);
            leaderGameButton.setVisible(true);
            stopGameButton.setVisible(true);
            
            gameStarted = true;
            
            world.setStyle("-fx-text-fill: rgba(0, 0, 0, 0.2)");
            continent.setStyle("-fx-text-fill: rgba(0, 0, 0, 0.2)");
            country.setStyle("-fx-text-fill: rgba(0, 0, 0, 0.2)");
            
            subRegions.setText("Sub-Regions: "+getDistricts(currentMap));
            
            unplayable.setVisible(false);
            mouseOverName.setVisible(false);
            districts.setVisible(false);
            highScore.setVisible(false);
            fastestTime.setVisible(false);
            fewestGuesses.setVisible(false);
            flagImage.setVisible(false);
            
            bottomScore.setVisible(false);
            bottomBestTime.setVisible(false);
            bottomLeastGuesses.setVisible(false);
            
            if(!continent.isVisible())
            {
                capitalGameButton.setDisable(true);
                flagGameButton.setDisable(true);
                leaderGameButton.setDisable(true);
            }
            if(country.isVisible())
            {
                flagGameButton.setDisable(true);
                leaderGameButton.setDisable(true);
            }
	});
        //HOVER OVER
        startButton.setOnMouseEntered(e -> {
           startButton.setGraphic(new ImageView((loadImage(START_BUTTON_MO_FILE_PATH))));
        });

        startButton.setOnMouseExited(e -> {
           startButton.setGraphic(new ImageView((loadImage(START_BUTTON_FILE_PATH))));
        });
        
//        stackPane.setOnKeyPressed(ke->{
//	    if(ke.getCode() == KeyCode.C)
//            {
//                regionsFound.setText("Regions Found: 33");
//                regionsLeft.setText("Regions Left: 1");
//            }
//	});
        exitButton.setStyle("-fx-background-color: transparent;");
	exitButton.setOnAction(e -> {
	    controller.processExitGameRequest();
	});
        exitButton.setOnMouseEntered(e -> {
           exitButton.setGraphic(new ImageView((loadImage(EXIT_BUTTON_MO_FILE_PATH))));
        });

        exitButton.setOnMouseExited(e -> {
           exitButton.setGraphic(new ImageView((loadImage(EXIT_BUTTON_FILE_PATH))));
        });
        
        nameGameButton.setStyle("-fx-background-color: transparent;");
        capitalGameButton.setStyle("-fx-background-color: transparent;");
        flagGameButton.setStyle("-fx-background-color: transparent;");
        leaderGameButton.setStyle("-fx-background-color: transparent;");
        stopGameButton.setStyle("-fx-background-color: transparent;");
        
	nameGameButton.setOnAction(e -> {
            gameState = "name";
	    controller.processStartGameRequest();
            regionsFound.setText("Regions Found: 0");
//            regionsLeft.setText("Regions Left: 34");
            gameStarted = true;
            capitalGameButton.setDisable(true);
            flagGameButton.setDisable(true);
            leaderGameButton.setDisable(true);
	});
        nameGameButton.setOnMouseEntered(e -> {
           nameGameButton.setGraphic(new ImageView((loadImage(NAME_GAME_BUTTON_MO_FILE_PATH))));
        });

        nameGameButton.setOnMouseExited(e -> {
           nameGameButton.setGraphic(new ImageView((loadImage(NAME_GAME_BUTTON_FILE_PATH))));
           if(gameState.equals("name"))
           {
               nameGameButton.setGraphic(new ImageView((loadImage(NAME_GAME_BUTTON_MO_FILE_PATH))));
               nameGameButton.setDisable(true);
           }
        });
        
        capitalGameButton.setOnAction(e -> {
            gameState = "capital";
	    controller.processStartGameRequest();
            regionsFound.setText("Regions Found: 0");
//            regionsLeft.setText("Regions Left: 34");
            gameStarted = true;
            nameGameButton.setDisable(true);
            flagGameButton.setDisable(true);
            leaderGameButton.setDisable(true);
	});
        capitalGameButton.setOnMouseEntered(e -> {
           capitalGameButton.setGraphic(new ImageView((loadImage(CAPITAL_GAME_BUTTON_MO_FILE_PATH))));
        });

        capitalGameButton.setOnMouseExited(e -> {
           capitalGameButton.setGraphic(new ImageView((loadImage(CAPITAL_GAME_BUTTON_FILE_PATH))));
           if(gameState.equals("capital"))
           {
               capitalGameButton.setGraphic(new ImageView((loadImage(CAPITAL_GAME_BUTTON_MO_FILE_PATH))));
               capitalGameButton.setDisable(true);
           }
        });
        
        flagGameButton.setOnAction(e -> {
            gameState = "flag";
            controller.processStartGameRequest();
            if(((RegioVincoDataModel)data).getStackState() == false)
            {
                regionsFound.setText("Regions Found: 0");
    //            regionsLeft.setText("Regions Left: 34");
                gameStarted = true;
                capitalGameButton.setDisable(true);
                nameGameButton.setDisable(true);
                leaderGameButton.setDisable(true);
            }

	});
        flagGameButton.setOnMouseEntered(e -> {
           flagGameButton.setGraphic(new ImageView((loadImage(FLAG_GAME_BUTTON_MO_FILE_PATH))));
        });

        flagGameButton.setOnMouseExited(e -> {
           flagGameButton.setGraphic(new ImageView((loadImage(FLAG_GAME_BUTTON_FILE_PATH))));
           if(gameState.equals("flag"))
           {
               flagGameButton.setGraphic(new ImageView((loadImage(FLAG_GAME_BUTTON_MO_FILE_PATH))));
               flagGameButton.setDisable(true);
           }
        });
        
        leaderGameButton.setOnAction(e -> {
            gameState = "leader";
	    controller.processStartGameRequest();
            regionsFound.setText("Regions Found: 0");
//            regionsLeft.setText("Regions Left: 34");
            gameStarted = true;
            nameGameButton.setDisable(true);
            flagGameButton.setDisable(true);
            capitalGameButton.setDisable(true);
	});
        leaderGameButton.setOnMouseEntered(e -> {
           leaderGameButton.setGraphic(new ImageView((loadImage(LEADER_GAME_BUTTON_MO_FILE_PATH))));
        });

        leaderGameButton.setOnMouseExited(e -> {
           leaderGameButton.setGraphic(new ImageView((loadImage(LEADER_GAME_BUTTON_FILE_PATH))));
            if(gameState.equals("leader"))
           {
               leaderGameButton.setGraphic(new ImageView((loadImage(LEADER_GAME_BUTTON_MO_FILE_PATH))));
               leaderGameButton.setDisable(true);
           }
        });
        
        stopGameButton.setOnAction(e -> {
            Alert stopGame = new Alert(AlertType.CONFIRMATION);
            stopGame.setContentText("Stop Game?");
            Optional<ButtonType> show = stopGame.showAndWait();
            if(show.get() == ButtonType.OK){
                if(data.won())
                {
                   data.endGameAsLoss();
                   if(!(currentRegion == null))
                   {
                       audio.stop(currentRegion);
                   }
                }
                

                nameGameButton.setGraphic(new ImageView((loadImage(NAME_GAME_BUTTON_FILE_PATH))));
                capitalGameButton.setGraphic(new ImageView((loadImage(CAPITAL_GAME_BUTTON_FILE_PATH))));
                flagGameButton.setGraphic(new ImageView((loadImage(FLAG_GAME_BUTTON_FILE_PATH))));
                leaderGameButton.setGraphic(new ImageView((loadImage(LEADER_GAME_BUTTON_FILE_PATH))));

                bottomScore.setVisible(true);
                bottomBestTime.setVisible(true);
                bottomLeastGuesses.setVisible(true);
                
                world.setStyle("-fx-text-fill: black");
                continent.setStyle("-fx-text-fill: black");
                country.setStyle("-fx-text-fill: black");
                
                ImageView winView = guiImages.get(WIN_DISPLAY_TYPE);
                winView.setVisible(false);
                winScoreLayer.setVisible(false);

                ImageView backingView = guiImages.get(BACK_TYPE);
                backingView.setVisible(false);

                ImageView coverView = guiImages.get(COVER_TYPE);
                coverView.setVisible(false);

                ImageView provinceView = guiImages.get(PROVINCE_TYPE);
                provinceView.setVisible(false);

                scoreLayer.setVisible(false);

                gameState = "off";

                nameGameButton.setDisable(false);
                capitalGameButton.setDisable(false);
                flagGameButton.setDisable(false);
                leaderGameButton.setDisable(false);

                nameGameButton.setVisible(false);
                capitalGameButton.setVisible(false);
                flagGameButton.setVisible(false);
                leaderGameButton.setVisible(false);
                stopGameButton.setVisible(false);

                startButton.setVisible(true);
                exitButton.setVisible(true);
                startExitLine.setVisible(true);



                String region = "";
                if(country.isVisible() == true)
                {
                    region = country.getText();
                }
                else
                {
                    if(continent.isVisible() == true)
                    {
                        region = continent.getText();
                    }
                    else
                    {
                        region = "The World";
                    }
                }
                ImageView mapView = guiImages.get(MAP_TYPE);
                mapView.setVisible(true);            
                gameStarted = false;
                data.resetMap(this, region);
                displayScoreFile(region);
                ((RegioVincoDataModel)data).clearStack();
                gameLayer.setVisible(false);
                labelCounter();
            }
            else
            {

            }
	});
        stopGameButton.setOnMouseEntered(e -> {
           stopGameButton.setGraphic(new ImageView((loadImage(STOP_GAME_BUTTON_MO_FILE_PATH))));
        });

        stopGameButton.setOnMouseExited(e -> {
           stopGameButton.setGraphic(new ImageView((loadImage(STOP_GAME_BUTTON_FILE_PATH))));
        });
        
        Button enterButton = guiButtons.get(ENTER_TYPE);
        enterButton.setStyle("-fx-background-color: transparent;");
	enterButton.setOnAction(e -> {
	    splashLayer.setVisible(false);
            data.resetMap(this, "The World");
            mapName.setText("WORLD");
            currentMap = "The World";
            displayScoreFile("The World");
            world.setVisible(true);
            //loadMapImage("Slovakia");
            //System.out.println("BLORRRB: " + ((RegioVincoDataModel) data).getSubRegionMappedToColor(makeColor(220, 220, 220)));
	});
        enterButton.setOnMouseEntered(e -> {
           enterButton.setGraphic(new ImageView((loadImage(ENTER_BUTTON_MO_FILE_PATH))));
        });

        enterButton.setOnMouseExited(e -> {
           enterButton.setGraphic(new ImageView((loadImage(ENTER_BUTTON_FILE_PATH))));
        });
        
        Button settingsButton = guiButtons.get(SETTINGS_TYPE);
        settingsButton.setStyle("-fx-background-color: transparent;");
	settingsButton.setOnAction(e -> {
	    settingsLayer.setVisible(true);
            helpLayer.setVisible(false);
	});
        //HOVER OVER
        settingsButton.setOnMouseEntered(e -> {
           settingsButton.setGraphic(new ImageView((loadImage(SETTINGS_BUTTON_MO_FILE_PATH))));
        });

        settingsButton.setOnMouseExited(e -> {
           settingsButton.setGraphic(new ImageView((loadImage(SETTINGS_BUTTON_FILE_PATH))));
        });
       
        Button helpButton = guiButtons.get(HELP_TYPE);
        helpButton.setStyle("-fx-background-color: transparent;");
	helpButton.setOnAction(e -> {
	     helpLayer.setVisible(true);
             settingsLayer.setVisible(false);
	});
        //HOVER OVER
        helpButton.setOnMouseEntered(e -> {
           helpButton.setGraphic(new ImageView((loadImage(HELP_BUTTON_MO_FILE_PATH))));
        });

        helpButton.setOnMouseExited(e -> {
           helpButton.setGraphic(new ImageView((loadImage(HELP_BUTTON_FILE_PATH))));
        });
        
        Button mapButton1 = guiButtons.get(MAP1_BUTTON_TYPE);
        mapButton1.setStyle("-fx-background-color: transparent;");
	mapButton1.setOnAction(e -> {
	    settingsLayer.setVisible(false);
            helpLayer.setVisible(false);
	});
        //HOVER OVER
        mapButton1.setOnMouseEntered(e -> {
           mapButton1.setGraphic(new ImageView((loadImage(MAP_BUTTON_MO_FILE_PATH))));
        });

        mapButton1.setOnMouseExited(e -> {
           mapButton1.setGraphic(new ImageView((loadImage(MAP_BUTTON_FILE_PATH))));
        });
        
        Button helpButton1 = guiButtons.get(HELP1_TYPE);
        helpButton1.setStyle("-fx-background-color: transparent;");
	helpButton1.setOnAction(e -> {
	     helpLayer.setVisible(true);
             settingsLayer.setVisible(false);
	});
        //HOVER OVER
        helpButton1.setOnMouseEntered(e -> {
           helpButton1.setGraphic(new ImageView((loadImage(HELP_BUTTON_MO_FILE_PATH))));
        });

        helpButton1.setOnMouseExited(e -> {
           helpButton1.setGraphic(new ImageView((loadImage(HELP_BUTTON_FILE_PATH))));
        });
        
        Button settingsButton2 = guiButtons.get(SETTINGS2_TYPE);
        settingsButton2.setStyle("-fx-background-color: transparent;");
	settingsButton2.setOnAction(e -> {
	    settingsLayer.setVisible(true);
            helpLayer.setVisible(false);
	});
        //HOVER OVER
        settingsButton2.setOnMouseEntered(e -> {
           settingsButton2.setGraphic(new ImageView((loadImage(SETTINGS_BUTTON_MO_FILE_PATH))));
        });

        settingsButton2.setOnMouseExited(e -> {
           settingsButton2.setGraphic(new ImageView((loadImage(SETTINGS_BUTTON_FILE_PATH))));
        });
        
        Button mapButton2 = guiButtons.get(MAP2_BUTTON_TYPE);
        mapButton2.setStyle("-fx-background-color: transparent;");
	mapButton2.setOnAction(e -> {
	    settingsLayer.setVisible(false);
            helpLayer.setVisible(false);
	});
        //HOVER OVER
        mapButton2.setOnMouseEntered(e -> {
           mapButton2.setGraphic(new ImageView((loadImage(MAP_BUTTON_MO_FILE_PATH))));
        });

        mapButton2.setOnMouseExited(e -> {
           mapButton2.setGraphic(new ImageView((loadImage(MAP_BUTTON_FILE_PATH))));
        });
        
        Button sound1 = guiButtons.get(SOUND1_TYPE);
        sound1.setStyle("-fx-background-color: transparent;");
        sound1.setOnAction(e -> {
            if(soundOn == true)
            {
                sound1.setGraphic(new ImageView((loadImage(SOUND_OFF_FILE_PATH))));
                soundOn = false;
                audio.stop(SUCCESS);
                audio.stop(FAILURE);
            }
            else
            {
                sound1.setGraphic(new ImageView((loadImage(SOUND_ON_FILE_PATH))));
                soundOn = true;
            }
        });
        
        Button sound2 = guiButtons.get(SOUND2_TYPE);
        sound2.setStyle("-fx-background-color: transparent;");
        sound2.setOnAction(e -> {
            if(musicOn == true)
            {
                sound2.setGraphic(new ImageView((loadImage(SOUND_OFF_FILE_PATH))));
                musicOn = false;
                audio.stop(TRACKED_SONG);
                audio.stop(AFGHAN_ANTHEM);
                if(!(currentRegion == null))
                {
                    audio.stop(currentRegion);
                }
            }
            else
            {
                sound2.setGraphic(new ImageView((loadImage(SOUND_ON_FILE_PATH))));
                musicOn = true;
                audio.play(TRACKED_SONG, true);
            }
        });
        
	// MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);

        world.setOnMouseClicked(e -> {
            mapName.setText("WORLD");
            currentMap = "The World";
            continent.setVisible(false);
            country.setVisible(false);
            data.resetMap(this, "The World");
            displayScoreFile("The World");
        });
        
        continent.setOnMouseClicked(e -> {
            mapName.setText(continent.getText().toUpperCase());
            currentMap = continent.getText();
            country.setVisible(false);
            data.resetMap(this, continent.getText());
            displayScoreFile(continent.getText());
        });
        
	// SETUP MOUSE PRESSES ON THE MAP
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
            if(gameStarted == true)
            {
                controller.processMapClickRequest((int) e.getX(), (int) e.getY());
                labelCounter();
            }
            else
            {
                if(!((RegioVincoDataModel)data).getSelectedColor((int) e.getX(), (int) e.getY()).equals(Color.PINK))
                {
                    String selectedRegion = (((RegioVincoDataModel)data).getSubRegionMappedToColor(((RegioVincoDataModel)data).getSelectedColor((int) e.getX(), (int) e.getY())));
                    mapName.setText(selectedRegion.toUpperCase());
                    currentMap = selectedRegion;
                    displayScoreFile(currentMap);
                    if(selectedRegion.equals("Africa") || selectedRegion.equals("Antarctica") || selectedRegion.equals("Asia") || selectedRegion.equals("Europe") || selectedRegion.equals("North America") || selectedRegion.equals("Oceania") || selectedRegion.equals("South America"))
                    {
                        continent.setText(selectedRegion);
                        continent.setVisible(true);
                        currentContinent = selectedRegion;
                    }
                    else
                    {
                        country.setText(selectedRegion);
                        country.setVisible(true);
                        currentRegion = selectedRegion;
                    }
                    data.resetMap(this, selectedRegion);
                }
                else if(((RegioVincoDataModel)data).getSelectedColor((int) e.getX(), (int) e.getY()).equals(Color.PINK))
                {
                        System.out.println("false");
                }
            }
	});
        mapView.setOnMouseMoved(e -> {
            if(gameStarted == false)
                {
                    if(((RegioVincoDataModel)data).getSelectedColor((int)e.getX(), (int)e.getY()).equals(Color.PINK))
                    {
                        unplayable.setVisible(true);
                        mouseOverName.setVisible(false);
                        districts.setVisible(false);
                        highScore.setVisible(false);
                        fastestTime.setVisible(false);
                        fewestGuesses.setVisible(false);
                        flagImage.setVisible(false);
                    }
                    else
                    {
                        //System.out.println(((RegioVincoDataModel)data).getSubRegionMappedToColor(((RegioVincoDataModel)data).getSelectedColor((int)e.getX(), (int)e.getY())));
                        if(((RegioVincoDataModel)data).getSubRegionMappedToColor(((RegioVincoDataModel)data).getSelectedColor((int)e.getX(), (int)e.getY())) != null)
                        {
                            unplayable.setVisible(false);
                            mouseOverName.setVisible(true);
                            districts.setVisible(true);
                            highScore.setVisible(true);
                            fastestTime.setVisible(true);
                            fewestGuesses.setVisible(true);
                            if(continent.isVisible())
                            {    
                                flagImage.setVisible(true);
                            }
                            updateHighScoreLabel((int) e.getX(), (int)e.getY());
                            readScoresFromFile(mouseOverName.getText());
                        }
                        else
                        {
                            unplayable.setVisible(false);
                            mouseOverName.setVisible(false);
                            districts.setVisible(false);
                            highScore.setVisible(false);
                            fastestTime.setVisible(false);
                            fewestGuesses.setVisible(false);
                            flagImage.setVisible(false);
                        }
                    } 
                }
        });
	
	// KILL THE APP IF THE USER CLOSES THE WINDOW
	window.setOnCloseRequest(e->{
	    controller.processExitGameRequest();
	});
    }

    /**
     * Called when a game is restarted from the beginning, it resets all game
     * data and GUI controls so that the game may start anew.
     */
    @Override
    public void reset() {
	// IF THE WIN DIALOG IS VISIBLE, MAKE IT INVISIBLE
        String region = "";
            if(country.isVisible() == true)
            {
                region = country.getText();
            }
            else
            {
                if(continent.isVisible() == true)
                {
                    region = continent.getText();
                }
                else
                {
                    region = "The World";
                }
            }
            data.reset(this, region);
        if(((RegioVincoDataModel)data).getStackState() == false)
        {
            ImageView winView = guiImages.get(WIN_DISPLAY_TYPE);
            winView.setVisible(false);

            winScoreLayer.setVisible(false);

            ImageView backingView = guiImages.get(BACK_TYPE);
            backingView.setVisible(true);

            ImageView coverView = guiImages.get(COVER_TYPE);
            coverView.setVisible(true);

            ImageView mapView = guiImages.get(MAP_TYPE);
            mapView.setVisible(true);

            ImageView provinceView = guiImages.get(PROVINCE_TYPE);
            provinceView.setVisible(true);

            scoreLayer.setVisible(true);

            // AND RESET ALL GAME DATA
            gameLayer.setVisible(true);
//            String region = "";
//            if(country.isVisible() == true)
//            {
//                region = country.getText();
//            }
//            else
//            {
//                if(continent.isVisible() == true)
//                {
//                    region = continent.getText();
//                }
//                else
//                {
//                    region = "The World";
//                }
//            }
//            data.reset(this, region);
            labelCounter();              
        }
    }

    /**
     * This mutator method changes the color of the debug text.
     *
     * @param initColor Color to use for rendering debug text.
     */
    public static void setDebugTextColor(Color initColor) {
//        debugTextColor = initColor;
    }

    /**
     * Called each frame, this method updates the rendering state of all
     * relevant GUI controls, like displaying win and loss states and whether
     * certain buttons should be enabled or disabled.
     */
    int backgroundChangeCounter = 0;

    @Override
    public void updateGUI() {
	// IF THE GAME IS OVER, DISPLAY THE APPROPRIATE RESPONSE
	if (data.won()) {
            
            regionName.setText("Region: " + currentMap);
	    ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
            ImageView provinceImage = guiImages.get(PROVINCE_TYPE);
            ImageView mapImage = guiImages.get(MAP_TYPE);
            ImageView coverView = guiImages.get(COVER_TYPE);
            ImageView backingView = guiImages.get(BACK_TYPE);
            backingView.setVisible(false);
            coverView.setVisible(false);
	    winImage.setVisible(true);
            provinceImage.setVisible(false);
            mapImage.setVisible(false);
            scoreLayer.setVisible(false);
            winScoreLayer.setVisible(true);
            gameDuration.setText("Game Duration: " + ((RegioVincoDataModel)data).getDuration() );
            String finalTime = ((RegioVincoDataModel)data).getDuration();
            wrongGuesses.setText("Incorrect Guesses: " + ((RegioVincoDataModel)data).getIncorrect());
            String finalIncorrect = ""+((RegioVincoDataModel)data).getIncorrect();
            int finalScore = ( ( (10000 - ((100*( ((RegioVincoDataModel)data).getIncorrect() )) + ((RegioVincoDataModel)data).getEndTime()) )));
            if(finalScore < 0)
            {
                finalScore = 0;
            }
            score.setText( "Score: " + finalScore);
            saveScoresToFile(currentMap, ""+finalScore, finalTime, finalIncorrect);
        }
    }

    public void labelCounter()
    {
        regionsFound.setText("Regions Found: " + ((RegioVincoDataModel)data).getRegionsFound());
        regionsLeft.setText("Regions Left: " + ((RegioVincoDataModel)data).getRegionsNotFound());
        incorrectGuesses.setText("Incorrect Guesses: " + ((RegioVincoDataModel)data).getIncorrect());
    }
    
    public void cCounter()
    {
        regionsFound.setText("Regions Found: " + ((getDistricts(currentMap))-1));
        regionsLeft.setText("Regions Left: 1");
    }
    
    public void reloadMap(String fileName) {
        //Image tempMapImage = loadImage("./data/maps/" + fileName + " Map.png");
        Image tempMapImage = loadImage("./data/The World/The World Map.png");
        if(fileName.equals("The World"))
        {
            tempMapImage = loadImage("./data/The World/The World Map.png");
        }
        else if(fileName.equals("Asia") || fileName.equals("Antarctica") || fileName.equals("Europe") || fileName.equals("Oceania") || fileName.equals("North America") || fileName.equals("South America") || fileName.equals("Africa"))
        {
            tempMapImage = loadImage("./data/The World/" + fileName + "/" + fileName + " Map.png");
        }
        else
        {
            tempMapImage = loadImage("./data/The World/" + continent.getText() + "/" + fileName + "/" + fileName + " Map.png");
        }
        
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
        
        //if(worldData.load(file))
        {
            HashMap<String, Region> map = worldData.getMap();
            for(HashMap.Entry<String, Region> entry : map.entrySet())
            {
                Region value = entry.getValue();
                for(int i = 0; i < mapImage.getWidth(); i++)
                {
                    for(int j = 0; j < mapImage.getHeight(); j++)
                    {
                        Color selected = pixelReader.getColor(i, j);
                        if(selected.equals(Color.web("dc6e00")))
                        {
                            mapImage.getPixelWriter().setColor(i, j, Color.TRANSPARENT);
                        }
                        //////////////////////////////////////////
                        if(gameState.equals("flag"))
                        {
                            if(selected.equals(makeColor(value.getRed(), value.getRed(), value.getRed())))
                            {
                                if(continent.isVisible())
                                {
                                    if(country.isVisible())
                                    {
                                       if(!new File("./data/The World/" + continent.getText() + "/" + value.getName() + "/" + value.getName() + " Flag.png").exists())
                                       {   
                                           mapImage.getPixelWriter().setColor(i, j, Color.PINK);
                                       } 
                                    }
                                    else
                                    {
                                       if(!new File("./data/The World/" + continent.getText() + "/" + value.getName() + "/" + value.getName() + " Flag.png").exists())
                                       {   
                                           mapImage.getPixelWriter().setColor(i, j, Color.PINK);
                                       } 
                                    }
                                }
                            }
                        }
                        //////////////////////////////////////////
                    }
                }
            }
        }
//        for(int i = 0; i < mapImage.getWidth(); i++)
//        {
//            for(int j = 0; j < mapImage.getHeight(); j++)
//            {
//                Color selected = pixelReader.getColor(i, j);
////                if(((RegioVincoDataModel) data).getSubRegionMappedToColor(selected) != null);
////                {
////                    System.out.println(((RegioVincoDataModel) data).getSubRegionMappedToColor(selected));
////                }
//            }
//        }
        
        ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	int numSubRegions = ((RegioVincoDataModel) data).getRegionsFound() + ((RegioVincoDataModel) data).getRegionsNotFound();
	this.boundaryTop = -(numSubRegions * 50);

	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((RegioVincoDataModel) data).setMapImage(mapImage);
        
    }
    public void setLabelStyle(Label label, int x, int y, String color)
    {
        label.translateXProperty().setValue(x);
        label.translateYProperty().setValue(y);
        label.setScaleX(2);
        label.setScaleY(2);
        label.setTextFill(Color.web(color));
    }
    public Pane getScoreLayer()
    {
        return scoreLayer;
    }
    
    public void loadMap(String name)
    {
            File file = new File("./data/xml/" + name + " Data.xml");
            if(worldData.load(file))
            {
                HashMap<String, Region> map = worldData.getMap();
                for (HashMap.Entry<String, Region> entry : map.entrySet()) {
                    Object value = entry.getValue();
                    System.out.println(" " + value);
                }
            }
            else
            {
                System.out.print("Could not load file");
            }
            
    }
    public void loadMapImage(String name) {
        
        
//        if(new File("./data/The World/" + name + " Map.png").exists())
//        {
        Image tempMapImage = loadImage("./data/The World/The World Map.png");
        if(name.equals("The World"))
        {
            tempMapImage = loadImage("./data/The World/The World Map.png");
        }
        else if(name.equals("Asia") || name.equals("Antarctica") || name.equals("Europe") || name.equals("Oceania") || name.equals("North America") || name.equals("South America") || name.equals("Africa"))
        {
            tempMapImage = loadImage("./data/The World/" + name + "/" + name + " Map.png");
        }
        else
        {
            tempMapImage = loadImage("./data/The World/" + continent.getText() + "/" + name + "/" + name + " Map.png");
        }
        
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
	
        File file = new File("./data/The World/The World Data.xml");
        if(name.equals("Asia") || name.equals("Antarctica") || name.equals("Europe") || name.equals("Oceania") || name.equals("North America") || name.equals("South America") || name.equals("Africa"))
        {
            file = new File("./data/The World/" + name + "/" + name + " Data.xml");
        }
        else if(country.isVisible())
        {
            file = new File("./data/The World/" + continent.getText() + "/" + name + " Data.xml");
        }
        if(worldData.load(file))
        {
//            if(!name.equals("Slovakia"))
            {
            HashMap<String, Region> map = worldData.getMap();
            for(HashMap.Entry<String, Region> entry : map.entrySet())
            {
                Region value = entry.getValue();
                for(int i = 0; i < mapImage.getWidth(); i++)
                {
                    for(int j = 0; j < mapImage.getHeight(); j++)
                    {
                        Color selected = pixelReader.getColor(i, j);
                        if(selected.equals(Color.web("dc6e00")))
                        {
                            mapImage.getPixelWriter().setColor(i, j, Color.TRANSPARENT);
                        }
                        if(selected.equals(makeColor(value.getRed(), value.getRed(), value.getRed())))
                        {
                            if(continent.isVisible() == false)
                            {    
                                if(!new File("./data/The World/" + value.getName() + "/" + value.getName() + " Data.xml").exists())
                                {   
                                    mapImage.getPixelWriter().setColor(i, j, Color.PINK);
                                }
                            }
                            else if(continent.isVisible())
                            {
                                if(country.isVisible())
                                {
                                   if(!new File("./data/The World/" + continent.getText() + "/" + value.getName() + "/" + value.getName() + " Data.xml").exists())
                                   {   
                                       mapImage.getPixelWriter().setColor(i, j, Color.PINK);
                                   } 
                                }
                                else
                                {
                                   if(!new File("./data/The World/" + continent.getText() + "/" + value.getName() + "/" + value.getName() + " Data.xml").exists())
                                   {   
                                       mapImage.getPixelWriter().setColor(i, j, Color.PINK);
                                   } 
                                }
                            }
                        }
                    }
                }
            }
            }
        }
//        for(int i = 0; i < mapImage.getWidth(); i++)
//        {
//            for(int j = 0; j < mapImage.getHeight(); j++)
//            {
//                Color selected = pixelReader.getColor(i, j);
//                if(selected.equals(makeColor(220, 220, 220)))
//                {
//                    mapImage.getPixelWriter().setColor(i, j, Color.PINK);
//                }
//            }
//        }
        
        ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	int numSubRegions = ((RegioVincoDataModel) data).getRegionsFound() + ((RegioVincoDataModel) data).getRegionsNotFound();
	this.boundaryTop = -(numSubRegions * 50);

	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((RegioVincoDataModel) data).setMapImage(mapImage);
        
//        File file1 = new File("./data/xml/" + name + " Data.xml");
//        if(worldData.load(file1))
//        {
//            HashMap<String, Region> map = worldData.getMap();
//            for(HashMap.Entry<String, Region> entry : map.entrySet())
//            {
//                Object value = entry.getValue();
//                System.out.println(value);
//            }
//        }
//        else
//        {
//            System.out.print("Could not load file");
//        }
//    }
    }
    public WorldDataManager getWorldDataManager()
    {
        return worldData;
    }
    public void updateHighScoreLabel(int x, int y)
    {
        int counter = 0;

        String selected = ((RegioVincoDataModel)data).getSubRegionMappedToColor(((RegioVincoDataModel)data).getSelectedColor(x, y));
        //File file = new File("./data/xml/" + selected + " Data.xml");
        File file = new File("./data/The World/The World Data.xml");
        if(selected.equals("Asia") || selected.equals("Antarctica") || selected.equals("Europe") || selected.equals("Oceania") || selected.equals("North America") || selected.equals("South America") || selected.equals("Africa"))
        {
            file = new File("./data/The World/" + selected + "/" + selected + " Data.xml");
        }
        else if(continent.isVisible() && (country.isVisible()==false))
        {
            file = new File("./data/The World/" + continent.getText() + "/" + selected + "/" + selected + " Data.xml");
            flagImage.setGraphic(new ImageView((loadImage("./data/The World/" + continent.getText() + "/" + selected + "/" + selected + " Flag.png"))));
        }
        if (file.exists())
        {
            if(worldData.load(file))
            {
                HashMap<String, Region> map = worldData.getMap();
                for(HashMap.Entry<String, Region> entry : map.entrySet())
                {
//                    Object value = entry.getValue();
//                    System.out.println(value);
                    counter++;
                }
            }
        }
        districts.setText("Districts: " + counter);

        mouseOverName.setText(((RegioVincoDataModel)data).getSubRegionMappedToColor(((RegioVincoDataModel)data).getSelectedColor(x, y)));
//        highScore.setText("");
//        fastestTime.setText("");
//        fewestGuesses.setText("");
    }
    public Boolean getSoundState()
    {
        return soundOn;
    }
    public Boolean getMusicState()
    {
        return musicOn;
    }
    public Boolean countryState()
    {
        return country.isVisible();
    }
    public Boolean continentState()
    {
        return continent.isVisible();
    }
    public String getCountry()
    {
        return country.getText();
    }
    public String getContinent()
    {
        return continent.getText();
    }
    public String getGameState()
    {
        return gameState;
    }
    public String getCurrentRegionSongFile()
    {
        return ("./data/The World/" + currentContinent + "/" + currentRegion + "/" + currentRegion + " National Anthem.mid");
    }
    
    public String getCurrentRegion()
    {
        return currentRegion;
    }
    public String getCurrentContinent()
    {
        return currentContinent;
    }
    public void saveScoresToFile(String regionName, String score, String time, String guesses){

        try
        {
            BufferedWriter output = null;
            File file = new File("./data/scores/"+ regionName +".txt");
            int bestScore = 0;
            int bestTime = 999;
            String oldTime = "";
            int leastGuesses = 9999;
            if(file.exists())
            {
                try 
                {
                    Scanner reader = new Scanner(file);
                    String result = "";
                    int lineN = 1;
                    while(reader.hasNextLine() && lineN <= 3)
                    {
                    result = reader.nextLine();
                    if(lineN == 1)
                    {
                        bestScore = Integer.parseInt(result);
                    }
                    if(lineN == 2)
                    {
                        bestTime = Integer.parseInt(result.charAt(0)+""+result.charAt(2)+""+result.charAt(3));
                        oldTime = result;
                    }
                    if(lineN == 3)
                    {
                        leastGuesses = Integer.parseInt(result);
                    }
                    lineN++;
                    }
                    reader.close();
                } 
                catch (Exception e)
                {
                }
            }
            if(bestScore < Integer.parseInt(score))
            {
                bestScore = Integer.parseInt(score);
            }
            if(bestTime > Integer.parseInt(time.charAt(0)+""+time.charAt(2)+""+time.charAt(3)))
            {
                oldTime = time;
            }
            if(leastGuesses > Integer.parseInt(guesses))
            {
                leastGuesses = Integer.parseInt(guesses);
            }
            output = new BufferedWriter(new FileWriter(file));

            output.write(bestScore + "\n");
            output.write(oldTime + "\n");
            output.write(leastGuesses + "\n");

            output.close();
        }
        catch(Exception e)
        {
            System.out.println("Could not create file");
        }
    
    }
    public void readScoresFromFile(String regionName)
    {
        File file = new File("./data/scores/"+ regionName +".txt");

        if(file.exists())
        {
            try 
            {
                Scanner reader = new Scanner(file);
                String result = "";
                int lineN = 1;
                while(reader.hasNextLine() && lineN <= 3)
                {
                result = reader.nextLine();
                if(lineN == 1)
                {
                    highScore.setText("Highscore: " + result);
                }
                if(lineN == 2)
                {
                    fastestTime.setText("Fastest Time: " + result);
                }
                if(lineN == 3)
                {
                    fewestGuesses.setText("Fewest Guesses: " + result);
                }
                lineN++;
                }
                reader.close();
            } 
            catch (Exception e)
            {
            }
        }
        else
        {
            highScore.setText("Highscore: N/A");
            fastestTime.setText("Fastest Time: N/A");
            fewestGuesses.setText("Fewest Guesses: N/A");
        }
    }
    public void displayScoreFile(String regionName)
    {
        File file = new File("./data/scores/"+ regionName +".txt");

        if(file.exists())
        {
            try 
            {
                Scanner reader = new Scanner(file);
                String result = "";
                int lineN = 1;
                while(reader.hasNextLine() && lineN <= 3)
                {
                result = reader.nextLine();
                if(lineN == 1)
                {
                    bottomScore.setText("Highscore: " + result);
                }
                if(lineN == 2)
                {
                    bottomBestTime.setText("Fastest Time: " + result);
                }
                if(lineN == 3)
                {
                    bottomLeastGuesses.setText("Fewest Guesses: " + result);
                }
                lineN++;
                }
                reader.close();
            } 
            catch (Exception e)
            {
            }
        }
        else
        {
            bottomScore.setText("Highscore: N/A");
            bottomBestTime.setText("Fastest Time: N/A");
            bottomLeastGuesses.setText("Fewest Guesses: N/A");
        }
    }
    public void changeFlagButton(boolean state)
    {
        Button flagGameButton = guiButtons.get(FLAG_GAME_TYPE);
        flagGameButton.setDisable(state);
    }
    public  int getDistricts(String selected)
    {
        int counter = 0;

        File file = new File("./data/The World/The World Data.xml");
        if(selected.equals("Asia") || selected.equals("Antarctica") || selected.equals("Europe") || selected.equals("Oceania") || selected.equals("North America") || selected.equals("South America") || selected.equals("Africa"))
        {
            file = new File("./data/The World/" + selected + "/" + selected + " Data.xml");
        }
        else if(continent.isVisible() && (country.isVisible()==false))
        {
            file = new File("./data/The World/" + continent.getText() + "/" + selected + "/" + selected + " Data.xml");
            flagImage.setGraphic(new ImageView((loadImage("./data/The World/" + continent.getText() + "/" + selected + "/" + selected + " Flag.png"))));
        }
        if (file.exists())
        {
            if(worldData.load(file))
            {
                HashMap<String, Region> map = worldData.getMap();
                for(HashMap.Entry<String, Region> entry : map.entrySet())
                {
//                    Object value = entry.getValue();
//                    System.out.println(value);
                    counter++;
                }
            }
        }
        return counter;
    }
}